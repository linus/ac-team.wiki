# 2022 roadmap

## Q4 - October to December 2022

* Sponsor 30
  * O2.3.1 - Develop new and/or improve existing bridge selection and distribution strategies based on data collected about successful, effective methods per evaluation during O1.1.
    * [conjure](https://gitlab.torproject.org/groups/tpo/anti-censorship/-/boards?label_name[]=Sponsor%2030
) (2 months) (cecylia) 

* [Sponsor 96](https://gitlab.torproject.org/groups/tpo/anti-censorship/-/boards?label_name[]=Sponsor%2096)
  * O1.1.1 Prepare the Snowflake system for a surge in operators and users.
    * proxies need to be updated to next version. <-- Q4 shell
    * setup new bridges.
  * O1.3: Implement bridges with pluggable transport HTTPT support. <--Q4(Shell)
  * O1.4: Increase the number of active obfs4 and HTTPT bridges. 
  * O1.4.3: Monitor bridge health.   <-- Meskio with Gus
    * obfsproxy security issues - <--- will go to icebox for now
  * O2.2: Deploy improved bridge distribution systems.
    * Salmon based design: Cecylia in Q4
    * rdsys DB for bridges (meskio) Q4
  * O2.3: React and steer our response to censorship. (Shell) - Q4
    * vantage points in specific places
  * O4.1: Localize all UI modified in this project. (meskio) Q4
    * Q4 Localize gettor. 
    * Q4 Bot Telegram.
  * O4.3: Modify GetTor so that it can distribute Tor Browser via messaging apps
    * get gettor into rdsys - Deployment in Q4.

* Sponsor 28 (itchyonion) & extension (shel)
  * probetest centralized log collection (shell) Q4
  * Raw Probe Log Data Collection

## Q3 - July to September 2022

* sponsor 30
  * O2.3.1 - Develop new and/or improve existing bridge selection and distribution strategies based on data collected about successful, effective methods per evaluation during O1.1.
    * conjure (2 months) (cecylia) <-- maintenance in Q3.
  * https://gitlab.torproject.org/groups/tpo/anti-censorship/-/boards?label_name[]=Sponsor%2030

* sponsor 96
  * O1.1.1 Prepare the Snowflake system for a surge in operators and users. <-- deployed snowflake broker in Q2.
    * next in Q3:
      * proxies need to be updated to next version.
      * setup new bridges.
  * O1.3: Implement bridges with pluggable transport HTTPT support. <--Q3 (Shell)
  * O1.4: Increase the number of active obfs4 and HTTPT bridges.
    * O1.4.3: Monitor bridge health.   <-- Meskio with Gus
      * obfsproxy security issues - meskio Q3
  * O2.2: Deploy improved bridge distribution systems.
    * Salmon based design: Roger on Q3.
    * rdsys DB for bridges (meskio) Q3
  * O2.3: React and steer our response to censorship. (Shell) - Q3
    * vantage points in specific places
  * O4.1: Localize all UI modified in this project. (meskio) Q3
    * Q3 Localize gettor.
    * Q3 Bot Telegram.
  * O4.3: Modify GetTor so that it can distribute Tor Browser via messaging apps
    * support onionsprout deployment - It was deployed in Q2
    * get gettor into rdsys - implemented in Q2. Deployment in Q3.
  * https://gitlab.torproject.org/groups/tpo/anti-censorship/-/boards?label_name[]=Sponsor%2096

* sponsor 28 (itchyonion) & extension (shel)
  * probetest centralized log collection (shell) Q3


## Q2 - April to June 2022

* sponsor 30 O2.3.1 - Develop new and/or improve existing bridge selection and distribution strategies based on data collected about successful, effective methods per evaluation during O1.1. - Implement and deploy conjure - Cecylia
* sponsor 96 O1.1.1 Prepare the Snowflake system for a surge in operators and users. - shel
* sponsor 96 O1.2: Increase the number of Snowflake bridges. - shel
* sponsor 96 O1.2.2: Scale Tor reachability through mobile Snowflakes.
* sponsor 96 O1.3: Implement bridges with pluggable transport HTTPT support.
* sponsor 96 O1.4: Increase the number of active obfs4 and HTTPT bridges.
* sponsor 96 O1.4.3: Monitor bridge health.   - Meskio with Gus
* sponsor 96 obfsproxy security issues - meskio
* sponsor 96  O2.2: Deploy improved bridge distribution systems.
* sponsor 96 Salmon based design :) --- Roger
* sponsor 96 O2.3: React and steer our response to censorship. - Shel
* sponsor 96 O4.3: Modify GetTor so that it can distribute Tor Browser via messaging apps - Meskio
* sponsor 96 get gettor into rdsys - Q2
* sponsor 28 - itchyonion
* sponsor 928 probetest centralized log collection (shel)
* sponsor 125 dynamic bridges


### Q1 - January to March 2022

* s30 O2.3.1 - Develop new and/or improve existing bridge selection and distribution strategies based on data collected about successful, effective methods per evaluation during O1.1.
* s30 conjure (2 months) - cecylia starting in March
  * server side : the university
  * start talking again with eric and his team
  * define server side for them to setup bridge (documentation)
  * client side : around 2 weeks
    * write a conjure client based on their specification (tor pt part with their client side library)
  * staging/testing
    * deployment
  * add it to alpha version of TB
  * metrics (discuss with people that maintain conjure bridge)
    * how many clients are connected

* s96 O1.1.1 Prepare the Snowflake system for a surge in operators and users. <-- scale in Q1/Q2 2022 <-- shell
* s96 O1.2: Increase the number of Snowflake bridges.
* s96 O1.2.2: Scale Tor reachability through mobile Snowflakes. <-- support to GP
* s96 O1.4: Increase the number of active obfs4 and HTTPT bridges.
* s96 O1.4.3: Monitor bridge health.   <-- Meskio/Shel with Gus
* s96 O2.1: Make it easier for humans & harder for censors to get bridges from moat distributor.  <-- Q1
* s96 O2.2: Deploy improved bridge distribution systems.
* s96 O2.2.2: Deploy next generation bridge distribution system (rdsys) <-- meskio -
* s96 O2.3: React and steer our response to censorship. <-- shel
* s96 O3.1: Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android). <-- meskio

* sponsor 28 & extension: we will have the new developer full time into this project.
* sponsor 28 - Improve the performance of Snowflake for users in Asia (cecylia)

You can follow up what we are working on in this [kanban board](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/anti-censorship/-/boards).


#2021 roadmap

<div>

<span dir="">## Review Q4 2021 - what did we do? </span>

</div>
<div>


</div>
<div>

<span dir="">(this were our goals for 2021)</span>

</div>
<div>

* <span dir="">~~Improve reputation based bridge distribution (e.g. Salmon)~~</span>

</div>
<div>

* <span dir="">~~since salmon still has some open research questions, we're now thinking that conjure is the more compelling option. still behind other items in priority though.~~</span>

</div>
<div>

* <span dir="">Better bridge distribution strategies (e.g. Conjure, Salmon) </span>

</div>
<div>

* <span dir="">conjure first priority <---this is the priority for s30 after rdsys is ready</span>

</div>
<div>

* <span dir="">salmon second priority Task 4.1 Let's build a plan for how we're going to start the Salmon research work (Task 4). <-- it gets move into 2022</span>

</div>
<div>

* <span dir="">Make GetTor more reliable and reduce maintenance burden</span>

</div>
<div>

* <span dir="">~~Rewrite gettor in Go to include it in rdsys (done)~~ <--- still using the old version</span>

</div>
<div>

* <span dir="">~~Evaluate if it makes sense to integrate the hard part of keeping gettor working in the past has been keeping all the files we distribute up-to-date. So let's not forget that part. :)~~</span>

</div>
<div>

* <span dir="">Complete integration of bridgedb into the more general rdsys -- continue in Q4 2021</span>

</div>
<div>

* <span dir="">need to get better at handling all the formats the bridge dir auth produces -- means extending the zoossh lib.</span>

</div>
<div>

* <span dir="">Improve the performance of Snowflake for users in Asia The destination we want is that users use multiple Snowflakes and it helps -- continue in Q4 2021</span>

</div>
<div>

* <span dir="">~~Cecylia was tuning the underlying KCP params.~~</span>

</div>
<div>

* <span dir="">~~Submitted patches to Shadow so Snowflake can now run inside Shadow.~~</span>

</div>
<div>

* <span dir="">Super important but it will take time. Could be tied into s96 and also s28.</span>

</div>
<div>

* <span dir="">The Shadow network model has a bunch of details about Tor relays, but not so many details about Snowflakes. So we need to extend Shadow's network model there.</span>

</div>
<div>

* [<span dir="">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026</span>](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026)

</div>
<div>

* <span dir="">~~Snowflake network health -- have enough of the right snowflakes, have volunteers happy Provide more feedback to users that run Snowflake proxies  -- continue in Q4 2021~~</span>

</div>
<div>

* <span dir="">~~We're doing well on our own monitoring, but haven't done much yet on helping users understand their contribution.~~</span>

</div>
<div>

* <span dir="">Tor reachability from various countries ~~Task 1.1 Automated scans to create a reachability dataset Task 1.1 Baselines before the blocking event -- get people used to running the tools and sending us the outputs. Maybe work with UX team to streamline this process, identify usability gaps in the tools.-- continue in Q4 2021~~</span>

</div>
<div>


</div>
<div>

* <span dir="">~~The Tor auto connect work, with the json censorship map and the new Tor Browser flow. -- continue in Q4 2021~~</span>

</div>
<div>


</div>
<div>


</div>
<div>


</div>
<div>

<span dir="">### MUST HAVE</span>

</div>
<div>


</div>
<div>

<span dir="">deliverables:</span>

</div>
<div>


</div>
<div>

* <span dir="">sponsor 30</span>

</div>
<div>

* <span dir="">O2.1.3 - Identify which bridge selection and distribution methods are most used in targeted regions.</span>

</div>
<div>

* <span dir="">O2.2 </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/7</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/7)

</div>
<div>

* <span dir="">O2.3.1 - Develop new and/or improve existing bridge selection and distribution strategies based on data collected about successful, effective methods per evaluation during O1.1.</span>

</div>
<div>

* <span dir="">~~3.3 automatic anti-censorship~~</span> [<span dir="">~~http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/15~~</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/15)

</div>
<div>


</div>
<div>

* <span dir="">sponsor 96</span>

</div>
<div>

* <span dir="">O1.1.1 Prepare the Snowflake system for a surge in operators and users.</span>

</div>
<div>

* [<span dir="">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026</span>](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026)

</div>
<div>

* [<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40066</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40066)

</div>
<div>

* <span dir="">  - Make Snowflake DoS contingency plan (see Aug 15 mail, "Subject: okr thoughts") </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/25593</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/25593)

</div>
<div>

* <span dir="">O2.2.2: Deploy next generation bridge distribution system (rdsys) <-- meskio</span>

</div>
<div>

* <span dir="">O2.3: React and steer our response to censorship. \[in China in particular\]</span>

</div>
<div>

* <span dir="">~~Needs to be decided who is going to take this work. Shel~~</span>

</div>
<div>

* <span dir="">Needs some scoping and planning.</span>

</div>
<div>

* <span dir="">Plausible to imagine that it will be a combination of Cecylia and Xiaokang, and then each of them will do other dev things too. Check with Xiaokang when he starts what he wants to focus on.</span>

</div>
<div>

* <span dir="">One challenge to keep in mind here: we need new distribution strategies, and/or PTs, because the ones we have today are not enough in China.</span>

</div>
<div>

* <span dir="">~~O3.1: Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android). <-- meskio~~</span>

</div>
<div>

* <span dir="">~~O3.2: Deploy Snowflake as a bridge option in Tor Browser stable.~~</span>

</div>
<div>

* <span dir="">O4.3: Modify GetTor so that it can distribute Tor Browser via messaging apps. - Start in November  or we can move to 2022. </span>

</div>
<div>

* <span dir="">E.g. Telegram (there is some partial code, it is not yet really finished)</span>

</div>
<div>

* <span dir="">Check with Gus and TGP about which messaging apps to use here. Need to pick one(s) that are both safe and popular, e.g. maybe not qq.</span>

</div>
<div>


</div>
<div>

* <span dir="">sponsor 28 & extension</span>

</div>
<div>

* <span dir="">~~Attend Snowflake surge of users~~</span>

</div>
<div>

* <span dir="">~~Be available for the October test event. (Most dev already done for it woo.)~~</span>

</div>
<div>

* <span dir="">~~Prep for and present at the December PI meeting (Dec 7-9)~~</span>

</div>
<div>

* <span dir="">~~Present a map of circumvention methods that work / are needed in each region.~~</span>

</div>
<div>

* [<span dir="">~~https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40009~~</span>](https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40009)

</div>
<div>

* <span dir="">Have data sets from our own measurements, and have shared them at least with other researchers. - we reached out to researches and are waiting on their replies</span>

</div>
<div>

* <span dir="">~~Have Snowflake performance test done with Shadow.~~</span>

</div>
<div>

* <span dir="">Have automated public graphs visualizing our China and Turkey and Canada results.</span>

</div>
<div>

* <span dir="">This will also help with awareness of whether the tests are still running.</span>

</div>
<div>

* [<span dir="">https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40020</span>](https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40020)

</div>
<div>

* <span dir="">Follow along with, and investigate, blocking events in the real world.</span>

</div>
<div>

* [<span dir="">https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40004</span>](https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40004)

</div>
<div>

* <span dir="">~~Add more tools to the suite of assessment tools we run.~~</span>

</div>
<div>

* <span dir="">~~Emma, marco, full vanilla Tor bootstrap, something for active probing.~~</span>

</div>
<div>

* <span dir="">Improve the output data format:</span>

</div>
<div>

* [<span dir="">https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40022</span>](https://gitlab.torproject.org/tpo/anti-censorship/censorship-analysis/-/issues/40022)

</div>
<div>


</div>
<div>

<span dir="">OKRs:</span>

</div>
<div>

* <span dir="">Make Tor accessible in China</span>

</div>
<div>

* <span dir="">improve the performance of Snowflake so that Tor bootstraps reliably on a mobile phone in China </span>[<span dir="">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026</span>](https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026)

</div>
<div>

* <span dir="">Detect and categorize attempts to censor Tor</span>

</div>
<div>

* <span dir="">deploy probes in areas that have/do/are likely to censor Tor and collect packet captures and probe results for storage and analysis</span>

</div>
<div>

* <span dir="">Improve the design and reliability of our software</span>

</div>
<div>

* <span dir="">add more user metrics-based monitoring and alert rules using prometheus</span>

</div>
<div>


</div>
<div>

<span dir="">### NICE TO HAVE</span>

</div>
<div>


</div>
<div>

* <span dir="">sponsor 30</span>

</div>
<div>

* <span dir="">starting with conjure</span>

</div>
<div>

* <span dir="">- Conjure integration progress?</span>

</div>
<div>


</div>
<div>

<span dir="">- Coordinate with other external research projects, to share plans and to try to get them in on helping us with our tasks:</span>

</div>
<div>

<span dir="">\[Roger would be happy to launch any of these conversations, but if we have no capacity to follow up on them, it seems sort of silly to launch them. But also we need to get other groups helping us, since we can't do everything ourselves. How to get out of being stuck?\]</span>

</div>
<div>

<span dir="">    - Nick Feamster wants to do data analysis on our censorship assessment data set</span>

</div>
<div>

<span dir="">    - Roya and Paul Pearce could run spooky-scan on the default bridge addresses</span>

</div>
<div>

<span dir="">    - Eric Wustrow wants to run obfs4+conjure for us. <---  end of this quarter or next quarter</span>

</div>
<div>

<span dir="">    - Jed Crandall's student wants to tell us about his circumvention ideas re VPNs</span>

</div>
<div>

<span dir="">    - Dave Levin (Geneva) wants to brainstorm how Geneva could be useful to us</span>

</div>
<div>

<span dir="">    - Ian Goldberg has a student working on a better Salmon design <----- next year</span>

</div>
<div>


</div>
<div>

<span dir="">- S28-extension tasks (should move some of these to MUST HAVE above):</span>

</div>
<div>

<span dir="">  - Coordinate with TPA/metrics to get a place to store the measurement dataset.</span>

</div>
<div>

<span dir="">  - Have a plan for how we're going to keep the docker installs up to date.</span>

</div>
<div>

<span dir="">  - Get a few more people running the docker image and generating data in their location</span>

</div>
<div>

<span dir="">  </span>

</div>
<div>

<span dir="">- Snowflake</span>

</div>
<div>

<span dir="">  - Provide more feedback to users that run Snowflake proxies</span>

</div>
<div>

  <span dir="">~~- We could integrate dcf's new amp cache implementation as an alternative to Fastly~~</span>

</div>
<div>


</div>
<div>


</div>
<div>

* <span dir="">Detect and categorize attempts to censor Tor</span>

</div>
<div>

* <span dir="">provide OONI with suggestions for improving the accuracy of OONI’s Tor tests</span>

</div>
<div>

* <span dir="">Improve the design and reliability of our software</span>

</div>
<div>

* <span dir="">add more user metrics-based monitoring and alert rules using prometheus</span>

</div>
<div>

* <span dir="">ensure that key infrastructure (BridgeDB, Snowflake broker, etc) can survive machine outages and restarts</span>

</div>
<div>

* <span dir="">remove "hacky shims" necessary for Moat</span>

</div>
<div>

* <span dir="">Release our data and software for use by the broader community</span>

</div>
<div>

* <span dir="">further improve the Snowflake library API to allow easy integration of Snowflake with other tools</span>

</div>
<div>

* <span dir="">complete our documentation for each of our tools so that other organizations can run their own anti-censorship infrastructure</span>

</div>
<div>


</div>
<div>


---

</div>
<div>


## <span dir="">Review Q3 - what did we do? </span>

</div>
<div>

* <span dir="">Make GetTor more reliable and reduce maintenance burden</span>
* <span dir="">Rewrite gettor in Go to include it in rdsys</span>
* <span dir="">Complete integration of bridgedb into the more general rdsys -- continue in Q4 2021</span>
* <span dir="">Improve the performance of Snowflake for users in Asia The destination we want is that users use multiple Snowflakes and it helps -- continue in Q4 2021</span>
  * <span dir="">Cecylia was tuning the underlying KCP params.</span>
  * <span dir="">Submitted patches to Shadow so Snowflake can now run inside Shadow.</span>
  * <span dir="">The Shadow network model has a bunch of details about Tor relays, but not so many details about Snowflakes. So we need to extend Shadow's network model there.</span>
* <span dir="">Snowflake network health -- have enough of the right snowflakes, have volunteers happy Provide more feedback to users that run Snowflake proxies  -- continue in Q4 2021</span>
  * <span dir="">We're doing well on our own monitoring, but haven't done much yet on helping users understand their contribution.</span>
* <span dir="">Tor reachability from various countries Task 1.1 Automated scans to create a reachability dataset Task 1.1 Baselines before the blocking event -- get people used to running the tools and sending us the outputs. Maybe work with UX team to streamline this process, identify usability gaps in the tools.-- continue in Q4 2021</span>
* <span dir="">The Tor auto connect work, with the json censorship map and the new Tor Browser flow. -- continue in Q4 2021</span>

### <span dir="">What worked well?</span>

* <span dir="">We did a bunch of really cool work</span>
* <span dir="">Meskio has jumped in and made a lot of progress on the s30 side of things</span>
* <span dir="">Coordination with other teams for the auto connect work.</span>

</div>\* <span dir="">S28 program manager remains happy with us. The s30/s96 ones too I think.</span> \* <span dir="">We have a promising third dev on the way. we hired other dev!  \\\\o/</span> \* <span dir="">We have actual results from our censorship assessment vantage points!</span>

<div>

### <span dir="">What could work better?</span>

</div>\* <span dir="">Cecylia is still a bottleneck on the racecar 'assessments' + race dev</span> \* <span dir="">We have a bunch of external groups who would love to coordinate with us but we don't have time/energy to start those coordination.</span> \* <span dir="">We have a lot of different irons in the fire right now and few people, so we are spread over many different code repositories and sponsor work and it's hard to prioritize between projects and keep up with new events</span> \\\* \* <span dir="">I don't know whether our censorship assessment vantage points are collecting data today or dead or what. :)</span> \\\* \* <span dir="">We have notifications from our monitoring infrastructure but we don't really have habits for how to react to the notifications.</span> \\\* \* <span dir="">Seems smart for us to change our notifications to only send email when it's actionable, e.g. if it's failed three tests in a row or something.</span> \\\* \* <span dir="">It remains tricky, especially with our limited capacity, to prioritize _between_ sponsors. Like, we do individual sponsor meetings, but it seems like all the prioritized tasks are for whichever sponsor meeting we just had. That thrashing results in a lot of changing priorities.</span> \\\* \* <span dir="">We will use the Thursday gaba-cecylia syncs to do a weekly check-in for the overall roadmap for the week.</span>

## <span dir="">ROADMAP 2021 Q4</span>

### <span dir="">MUST HAVE</span>

<div>

* <span dir="">sponsor 30</span>
  * <span dir="">O2.1.3 - Identify which bridge selection and distribution methods are most used in targeted regions.</span>
  * <span dir="">O2.2 </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/7</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/7)
  * <span dir="">O2.3.1 - Develop new and/or improve existing bridge selection and distribution strategies based on data collected about successful, effective methods per evaluation during O1.1.</span>
  * <span dir="">3.3 automatic anti-censorship </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/15</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/groups/tpo/-/milestones/15)

</div>
<div>

</div>
<div>

* <span dir="">sponsor 96</span>
  * <span dir="">O1.1.1 Prepare the Snowflake system for a surge in operators and users.</span>
    * [<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026)
    * [<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40066</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40066)
    * <span dir="">Make Snowflake DoS contingency plan (see Aug 15 mail, "Subject: okr thoughts") </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/25593</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/25593)
  * <span dir="">O2.2.2: Deploy next generation bridge distribution system (rdsys) <-- meskio</span>
  * <span dir="">O2.3: React and steer our response to censorship. \[in China in particular\]</span>
    * <span dir="">Needs to be decided who is going to take this work.</span>
    * <span dir="">Needs some scoping and planning. Plausible to imagine that it will be a combination of Cecylia and new-dev, and then each of them will do other dev things too. Check with new-dev when he starts what he wants to focus on.</span>
    * <span dir="">One challenge to keep in mind here: we need new distribution strategies, and/or PTs, because the ones we have today are not enough in China.</span>
  * <span dir="">O3.1: Improve automatic censorship detection during bootstrapping in Tor Browser (desktop and Android). <-- meskio</span>
  * <span dir="">~~O3.2: Deploy Snowflake as a bridge option in Tor Browser stable.~~</span>
  * <span dir="">O4.3: Modify GetTor so that it can distribute Tor Browser via messaging apps. - Start in November  or we can move to 2022. </span>
    * <span dir="">E.g. Telegram (there is some partial code, it is not yet really finished)</span>
    * <span dir="">Check with Gus and TGP about which messaging apps to use here. Need to pick one(s) that are both safe and popular, e.g. maybe not qq.</span>

</div>
<div>

</div>
<div>

* <span dir="">sponsor 28 & extension</span>
  * <span dir="">Attend Snowflake surge of users</span>
  * <span dir="">Be available for the October test event. (Most dev already done for it woo.)</span>
  * <span dir="">Prep for and present at the December PI meeting (Dec 7-9)</span>
  * <span dir="">Present a map of circumvention methods that work / are needed in each region. </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40009</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40009)
  * <span dir="">Have data sets from our own measurements, and have shared them at least with other researchers.</span>
  * <span dir="">Have Snowflake performance test done with Shadow.</span>
  * <span dir="">Have automated public graphs visualizing our China and Turkey and Canada results.</span>
  * <span dir="">This will also help with awareness of whether the tests are still running. </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40020</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40020)
  * <span dir="">Follow along with, and investigate, blocking events in the real world. </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40004</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40004)
  * <span dir="">Add more tools to the suite of assessment tools we run.</span>
    * <span dir="">Emma, marco, full vanilla Tor bootstrap, something for active probing.</span>
  * <span dir="">Improve the output data format:</span>

    [<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40022</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/censorship-analysis/-/issues/40022)

</div>
<div>

</div>
<div>

<span dir="">OKRs:</span>

</div>
<div>

* <span dir="">Make Tor accessible in China</span>
* <span dir="">Improve the performance of Snowflake so that Tor bootstraps reliably on a mobile phone in China </span>[<span dir="">http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026</span>](http://eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40026)
* <span dir="">Detect and categorize attempts to censor Tor</span>
* <span dir="">Deploy probes in areas that have/do/are likely to censor Tor and collect packet captures and probe results for storage and analysis</span>
* <span dir="">Improve the design and reliability of our software</span>
* <span dir="">Add more user metrics-based monitoring and alert rules using prometheus</span>

</div>
<div>

</div>
<div>

### <span dir="">NICE TO HAVE</span>

</div>
<div>

</div>
<div>

* <span dir="">sponsor 30</span>
  * <span dir="">starting with conjure</span>
* <span dir="">Coordinate with other external research projects, to share plans and to try to get them in on helping us with our tasks:</span>
* <span dir="">Roger would be happy to launch any of these conversations, but if we have no capacity to follow up on them, it seems sort of silly to launch them. But also we need to get other groups helping us, since we can't do everything ourselves. How to get out of being stuck?</span>
  * <span dir="">data analysis on our censorship assessment data set</span>
  * <span dir="">run spooky-scan on the default bridge addresses</span>
  * <span dir="">run obfs4+conjure for us. <---  end of this quarter or next quarter</span>
  * <span dir="">student wants to tell us about his circumvention ideas re VPNs</span>
  * <span dir="">Geneva wants to brainstorm how Geneva could be useful to us</span>
  * <span dir="">Student working on a better Salmon design <----- next year</span>
* <span dir="">S28-extension tasks (should move some of these to MUST HAVE above):</span>
  * <span dir="">Coordinate with TPA/metrics to get a place to store the measurement dataset.</span>
  * <span dir="">Have a plan for how we're going to keep the docker installs up to date.</span>
  * <span dir="">Get a few more people running the docker image and generating data in their location</span>
* <span dir="">Snowflake</span>
  * <span dir="">Provide more feedback to users that run Snowflake proxies</span>
  * <span dir="">We could integrate dcf's new amp cache implementation as an alternative to Fastl</span>
* <span dir="">Detect and categorize attempts to censor Tor</span>
  * <span dir="">Provide OONI with suggestions for improving the accuracy of OONI’s Tor tests</span>

</div>
<div>

* <span dir="">Improve the design and reliability of our software</span>
  * <span dir="">add more user metrics-based monitoring and alert rules using prometheus</span>
  * <span dir="">ensure that key infrastructure (BridgeDB, Snowflake broker, etc) can survive machine outages and restarts</span>
  * <span dir="">remove "hacky shims" necessary for Moat</span>

</div>
<div>

* <span dir="">Release our data and software for use by the broader community</span>
  * <span dir="">further improve the Snowflake library API to allow easy integration of Snowflake with other tools</span>
  * <span dir="">complete our documentation for each of our tools so that other organizations can run their own anti-censorship infrastructure</span>

</div>