We hold an occasional anti-censorship reading group discussion at the tail end of our weekly meetings. Currently our weekly meetings are at 16UTC in the #tor-meeting channel on the OFTC IRC server. Check out this guide for more information on how to connect: https://gitlab.torproject.org/legacy/trac/-/wikis/org/onboarding/IRC

# Upcoming Readings Schedule

Our next reading group discussion will be on 2021-11-21

Topic: [Measuring QQMail's automated email censorship in China](https://dl.acm.org/doi/10.1145/3473604.3474560)

# Past Readings

- [Characterizing Transnational Internet Performance and the Great Bottleneck of China](https://dl.acm.org/doi/pdf/10.1145/3379479)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2021/tor-meeting.2021-10-28-16.00.html

- [Exploring Simple Detection Techniques for DNS-over-HTTPS Tunnels](https://dl.acm.org/doi/10.1145/3473604.3474563)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2021/tor-meeting.2021-10-07-16.01.html

- [BlindTLS: Circumventing TLS-based HTTPS censorship](https://dl.acm.org/doi/10.1145/3473604.3474564)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2021/tor-meeting.2021-09-23-15.59.html

- [Domain Shadowing: Leveraging Content Delivery Networks for Robust Blocking-Resistant Communications](https://www.usenix.org/system/files/sec21fall-wei.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2021/tor-meeting.2021-04-29-16.00.html

- [Investigating Large Scale HTTPS Interception in
Kazakhstan](https://censoredplanet.org/assets/Kazakhstan.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2021/tor-meeting.2021-03-04-15.58.html

- [Early Detection of Censorship Events with Psiphon
Network Data](https://tics.site/proceedings/2019a/icn_2019_7_10_38005.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2021/tor-meeting.2021-01-21-15.58.html

- [Poking a Hole in the Wall: Efficient
Censorship-Resistant Internet Communications by Parasitizing on WebRTC](https://dl.acm.org/doi/pdf/10.1145/3372297.3417874)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-12-03-15.59.html

- [The use of TLS in Censorship Circumvention](https://tlsfingerprint.io/static/frolov2019.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-11-12-15.59.html

- [HTTPT: A Probe-Resistant Proxy](https://censorbib.nymity.ch/pdf/Frolov2020b.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-09-10-15.58.html

- [Turbo Tunnel](https://www.bamsoftware.com/papers/turbotunnel/turbotunnel.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-08-27-16.00.html

- [Emotional and Practical Considerations Towards the Adoption and
Abandonment of VPNs as a Privacy-Enhancing Technology](https://petsymposium.org/2020/files/papers/issue1/popets-2020-0006.pdf)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-07-23-15.58.html

- [GoodbyeDPI](https://github.com/ValdikSS/GoodbyeDPI)

  Meeting notes: https://lists.torproject.org/pipermail/tor-project/2020-July/002923.html

- [Geneva](https://censorbib.nymity.ch/#Bock2019a)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-06-25-15.58.html

- [V2Ray](https://www.v2ray.com/en/)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-06-11-15.59.html

- [Conjure: Summoning Proxies from Unused Address Space](https://censorbib.nymity.ch/#Frolov2019b)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-05-28-16.00.html

- [Salmon: Robust Proxy Distribution for Censorship Circumvention](https://censorbib.nymity.ch/#Douglas2016a)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-05-14-16.00.html

- [ MassBrowser: Unblocking the Censored Web for the Masses, by the Masses](https://censorbib.nymity.ch/#Nasr2020a)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-04-30-17.59.html

- [SymTCP: Eluding Stateful Deep Packet Inspection with Automated Discrepancy Discovery](https://censorbib.nymity.ch/#Wang2020a)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-04-16-17.59.html

- [Detecting Probe-Resistant Proxies](https://censorbib.nymity.ch/#Frolov2020a)

  Meeting notes: http://meetbot.debian.net/tor-meeting/2020/tor-meeting.2020-04-02-17.59.log.html