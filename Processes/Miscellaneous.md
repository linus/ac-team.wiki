## Labels and milestones

* *All* issues related to sponsors should have a sponsor label, e.g., "Sponsor 30". Issues that *must* be completed for a sponsor should also have a milestone associated with them.

## Roadmap

* Our team roadmap is [available here](https://gitlab.torproject.org/groups/tpo/anti-censorship/-/boards). Whatever you are currently working on (sponsor and non-sponsor work) should go into the "Doing" board. Whatever you intend on tackling throughout the current roadmapping period (which covers three months) should go into the "Next" board.

## Reviews

* When you have code to review, create a merge request – ideally from your personal to the canonical repository. When creating the merge request, assign it to whoever should review your code.

* We generally assign reviews once a week, as part of our [anti-censorship meeting](https://gitlab.torproject.org/tpo/anti-censorship/team#irc-meetings-schedule), but feel free to ping someone to review your code before our meeting, to get an expedited review. Our goal is to finish reviews in less than three days.

* If you want somebody to review anything that is not code, tag them in the issue, so the issue shows up in their [todo list](https://gitlab.torproject.org/dashboard/todos), e.g., "@phw, please review this". You can use the tag @tpo/anti-censorship to reach all team members but please use it sparingly.