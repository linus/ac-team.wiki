# Inspiration for naming your new pluggable transport.

* Kaizo
* Auger
* Windhover
* ~~Emma~~
* ~~obfs2~~
* ~~obfs3~~
* ~~obfs4~~
* obfs5
* ~~Turbo Tunnel (turbot for short)~~
* Fleabane
* Twister
* Moshpit
* ~~Sharknado~~
* Brekekekex-koax-koax
* Demeter
* For a DNS-based pluggable transport:
  * Dinosaur
  * Dienstag
* [North Star / Polaris](tpo/anti-censorship/pluggable-transports/snowflake#40158)

# Rhyming words in english that suggest confusion:

* addle
* riddle
* muddle
* twaddle
* twiddle
* fuddle
* [tweetle beetle paddle battle](https://www.youtube.com/watch?v=S82jwZ0pD1k)
* [meddle](https://archive.org/details/06.Echoes_201702)
* riddle
* skedaddle
* squiddle
* swizzle
* razzle
* sizzle
* dazzle
* puzzle

# Classic ciphers you wouldn't want to use in production (except *)

* playfair
* bacon
* vigenere
* alberti
* adfgvx
* bifid
* polybius
* onetime  *
* [onemoretime](https://archive.org/details/DAFT_PUNK_One_More_Time_2000)
* grille
* scytale
* caesar
* pigpen
* rot13

# Cryptids

* yeti
* sasquatch
* jerseydevil
* nessie
* dropbear
* chessie
* bigfoot
* chupacabra
* mothman

# Other sources:

* https://en.wikipedia.org/wiki/List_of_cryptozoologists
* https://en.wikipedia.org/wiki/List_of_cloud_types

# Things you can do to food with a knife:

* slice
* chop
* chiffonade
* julienne
* mince
* dice
* parmentier
* brunoise
* paysanne
* also see https://www.thecleaverquarterly.com/stories/chinese-knife-skills
* also see cooking/knife terms in any language you like.

# Settings on a blender

* puree
* frappe
* chop
* grind
* icecrush
* liquify
* off